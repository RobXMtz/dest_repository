﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour  // MAnager for Ikaruga like game ship
{

   //dan EI 5068664984


    Vector3 Movement;                                     // Input from keyboard
    float MaxSpeed = 20f;

    //public Vector2 ShootLocation;                               // Shooting variables 
    public Vector3[] ShootLocation;
    public GameObject[] Bullets;
    public float RateofFire;
    public int FireLevel;
    int LeftorRightMove;

    //float ZRotation;
    //Quaternion OriginalPos;
    //Quaternion MaxLeft;
    //Quaternion MaxRight;

    // for boost/dodge
    //float BoostTimer = 0.0f;
    //float BoostDelay = 0.2f;

    Rigidbody SpaceshipRB;



    // Start is called before the first frame update
    void Start()
    {
        RateofFire = 0.1f;
        SpaceshipRB = GetComponent<Rigidbody>();

        Bullets = new GameObject[3];
        Bullets [0] = Resources.Load("Prefabs/Fireball", typeof(GameObject)) as GameObject;
        Bullets [1] = Resources.Load("Prefabs/Fireball", typeof(GameObject)) as GameObject;
        ShootLocation = new Vector3[3];
        ShootLocation[0] = new Vector3(1, 1 ,1);
        ShootLocation[1] = new Vector3(1, 1 ,1);
        ShootLocation[2] = new Vector3(1, 1, 1);
        FireLevel = 2;
        LeftorRightMove = 0;
        //ZRotation = 0;
        //OriginalPos = Quaternion.Euler(180, 0, 180);
        //MaxLeft = Quaternion.Euler(180, 0, 140);
        //MaxRight = Quaternion.Euler(180, 0,220);
        //if(ShootLocation[0] != null)
        //{
        //    print("array initialized " + ShootLocation[0].x + "  " + ShootLocation[0].y);
        //    print("array initialized " + ShootLocation[1].x + "  " + ShootLocation[1].y);
        //    print("array initialized " + ShootLocation[2].x + "  " + ShootLocation[2].y);
        //}
        //else
        //{
        //    print("array unsuccessful");
        //}
        //if (Bullets[0] != null)
        //{
        //    print("file loaded");
        //}
        //else
        //{
        //    print("file not found");
        //}


    }

    // Update is called once per frame
    void Update()                                   // update is for input
    {

        CheckInputs();

    }

    void FixedUpdate()                              //fixed update is for physics
    {

        CheckMovement();

    }

    void CheckInputs()
    {
        if(Input.GetKeyDown(KeyCode.A))                 // need to know which key was pressed last so when both keys are pressed, the last one takes precedence
        {
            LeftorRightMove = -1;
        }
        else if(Input.GetKeyDown(KeyCode.D))
        {
            LeftorRightMove = +1;
        }
        if(Input.GetKey(KeyCode.Return))
        {
            Fire();
        }
        //if(Input.GetKey(KeyCode.Space))
        //{
        //    Boost();
        //}

    }

    void Fire()
    {
            RateofFire -= Time.deltaTime;
        if(FireLevel == 1)
        {

            ShootLocation[0] = new Vector2(transform.position.x, transform.position.y + 0.4f);                         //offset so the shor begins at the tip of the ship for level 1 shot
            if (RateofFire <= 0)
            {

                GameObject BulletClone;
                BulletClone = Instantiate(Bullets[1], ShootLocation[0], Quaternion.identity) as GameObject;
                RateofFire = 0.05f;
            }
        }
        else
        {
            if (RateofFire <= 0)
            {

                ShootLocation[1] = new Vector2(transform.position.x - 0.8f, transform.position.y + 0.2f);
                // print("bulletclone 1 location " + ShootLocation[0].x + "  " + ShootLocation[0].y);
                ShootLocation[2] = new Vector2(transform.position.x + 0.8f, transform.position.y + 0.2f);
                GameObject BulletClone;
                BulletClone = Instantiate(Bullets[0], ShootLocation[1], Quaternion.identity) as GameObject;
                // print("bulletclone 1 shot location " + ShootLocation[0].x + "  " + ShootLocation[0].y);
                BulletClone = Instantiate(Bullets[0], ShootLocation[2], Quaternion.identity) as GameObject;
                RateofFire = 0.05f;
            }

        }

    }

    //void Boost()
    //{

    //}


    void CheckMovement()
    {

        //temp parameters for controller deadzone 0.12
        //PS4 Controller

        Movement.x = Input.GetAxis("PS4_Horizontal");
        Movement.z = Input.GetAxis("PS4_Vertical");
        //print("movemen.x =  " + Movement.x);
  
        SpaceshipRB.velocity = new Vector3(Movement.x * MaxSpeed, 0.0f/*Movement.z * MaxSpeed*/, Movement.z * MaxSpeed);
       





        //backup of original controls in case i screw up
        //if (((Input.GetAxis("PS4_Horizontal") >= 0.2f || Input.GetAxis("PS4_Horizontal") <= -0.2f)) || ((Input.GetAxis("PS4_Vertical") >= 0.2f || Input.GetAxis("PS4_Vertical") <= -0.2f)))
        //{
        //    Movement.x = Input.GetAxis("PS4_Horizontal");
        //    Movement.z = Input.GetAxis("PS4_Vertical");
        //    SpaceshipRB.velocity = new Vector3(Movement.x * MaxSpeed, 0.0f/*Movement.z * MaxSpeed*/, Movement.z * MaxSpeed);
        //    // ZRotation = Quaternion.Angle(SpaceshipRB.transform.rotation, OriginalPos);

        //    if (ZRotation <= 40.0 && ZRotation >= -40.0)
        //    {

        //        SpaceshipRB.transform.Rotate(0.0f, 0.0f, Input.GetAxis("PS4_Horizontal") * 4, Space.Self);
        //    }
        //    else
        //    {
        //        SpaceshipRB.transform.rotation = Quaternion.Slerp(this.transform.rotation, OriginalPos, Time.deltaTime );
        //        ZRotation = 40.0f;
        //    }

        //}
        //else
        //{
        //    SpaceshipRB.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        //    SpaceshipRB.transform.rotation = Quaternion.Slerp(this.transform.rotation, OriginalPos, Time.deltaTime * MaxSpeed);
        //}
        //ZRotation = Quaternion.Angle(SpaceshipRB.transform.rotation, OriginalPos);


        ////controller for keybard WASD
        //if ((Input.GetKey(KeyCode.A) == true) || (Input.GetKey(KeyCode.D) == true))
        //{

        //    Movement.x = Input.GetAxisRaw("Horizontal");

        //}
        //else
        //{
        //    Movement.x = Input.GetAxis("Horizontal");
        //}
        //if ((Input.GetKey(KeyCode.W) == true) || (Input.GetKey(KeyCode.S) == true))
        //{

        //    Movement.y = Input.GetAxisRaw("Vertical");

        //}
        //else
        //{
        //    Movement.y = Input.GetAxis("Vertical");
        //}

        //if (Input.GetKey(KeyCode.A) == true && Input.GetKey(KeyCode.D) == true)
        //{
        //    SpaceshipRB.velocity = new Vector3(LeftorRightMove * MaxSpeed,0.0f,  Movement.y * MaxSpeed);
        //}
        //else
        //{
        //    SpaceshipRB.velocity = new Vector3(Movement.x * MaxSpeed, 0.0f,  Movement.y * MaxSpeed);
        //}

    }


}
