﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationScript : MonoBehaviour
{

    Rigidbody SpaceshipRB;
    float ZRotation;
    Quaternion OriginalPos;
    Quaternion MaxLeft;
    Quaternion MaxRight;

    float MaxSpeed = 20f;
    // Start is called before the first frame update
    void Start()
    {
        SpaceshipRB = GetComponent<Rigidbody>();
        ZRotation = 0;
        OriginalPos = Quaternion.Euler(180, 0, 180);
        MaxLeft = Quaternion.Euler(180, 0, 140);
        MaxRight = Quaternion.Euler(180, 0, 220);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckRotate();
    }

    void CheckRotate()
    {
        if (Input.GetAxis("PS4_Horizontal") >= 0.12f)                           // 0.12 is for dead controller margin 
        {
            if (ZRotation <= 40.0)
            {

                SpaceshipRB.transform.rotation = Quaternion.Slerp(this.transform.rotation, MaxRight, Time.deltaTime * MaxSpeed);
                //SpaceshipRB.transform.Rotate(0.0f, 0.0f, Input.GetAxis("PS4_Horizontal") * 4, Space.Self);
            }

        }
        else if (Input.GetAxis("PS4_Horizontal") <= -0.12f)
        {
            if (ZRotation >= -40.0)
            {

                SpaceshipRB.transform.rotation = Quaternion.Slerp(this.transform.rotation, MaxLeft, Time.deltaTime * MaxSpeed);
            }
        }
        else
        {
            SpaceshipRB.transform.rotation = Quaternion.Slerp(this.transform.rotation, OriginalPos, Time.deltaTime * MaxSpeed);
        }
        ZRotation = Quaternion.Angle(SpaceshipRB.transform.rotation, OriginalPos);
    }
}
