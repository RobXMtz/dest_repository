﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballScript : MonoBehaviour
{

    
    Vector2 Dir;
    // Start is called before the first frame update
    void Start()
    {
        Dir = new Vector2(0, 10f);
        GetComponent<Rigidbody2D>().velocity = new Vector2(Dir.x, Dir.y);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
