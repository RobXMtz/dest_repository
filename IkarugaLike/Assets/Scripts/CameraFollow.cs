﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    Vector3 StartPos;
    Vector3 CurrentPos;
    Vector3 PosOffset;
    Vector3 Endpos;
    Transform target;
    float TimeOffset;
    // Start is called before the first frame update
    void Start()
    {
        TimeOffset = 3.0f;
        StartPos = transform.position;
        target = GameObject.FindGameObjectWithTag("Player").transform;
        PosOffset = new Vector3(0f, 10f, 0f);
        Endpos = new Vector3(0f, 0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        CurrentPos = transform.position;
        Endpos = target.transform.position;
        Endpos.x += PosOffset.x;
        Endpos.y += PosOffset.y;
        Endpos.z += PosOffset.z;

        transform.position = Vector3.Lerp(CurrentPos, Endpos, TimeOffset * Time.deltaTime);

    }
}
